# Commandes pour la TP synthèse bash

> Ce fichier contient toutes les commandes effectuées pour se rapprocher le plus possible du résultat attendu. Si besoin, toutes les captures d'écran se trouvent individuellement dans le répertoire screenshots/

1)
```bash
du -h rep_etude/
```
![Listing de tous les fichiers et répertoires, et sous-répertoires du dossier rep_etude](screenshots/1.png)

2)
```bash
du -sh rep_etude/
# Ou
du -h rep_etude/ | tail -n 1
```
![Taille totale du dossier rep_etude](screenshots/2.png)

3)
```bash
tree rep_etude/ | wc -l
```
![Nombre de fichiers et répertoires et sous-répertoires de rep_etude sans distinguer les fichiers des dossiers](screenshots/3.png)

4)
```bash
tree rep_etude/ | tail -n 1
```
![Nombre de fichiers et répertoires et sous-répertoires de rep_etude en distinguant les fichiers des dossiers](screenshots/4.png)

5)
```bash
ls -R rep_etude/ | grep -E *.jpg
```
![Listing de tous les fichiers dans l'arborescence se terminant pas l'extension .jpg](screenshots/5.png)

6)
```bash
ls -R rep_etude/ | grep -E *".(jpg|png)"
```
![Listing de tous les fichiers dans l'arborescence se terminant par les extensions .jpg et .png](screenshots/6.png)

7)
```bash
ls -R rep_etude/ | grep -Ei ".jpg"$
```
![Listing de tous les fichiers dans l'arborescence se terminant par .jpg quelle que soit la casse des caractères "j", "p" et "g"](screenshots/7.png)

8)
```bash
mkdir OutPuts/ && for picture in `find rep_etude/ -type f -iname "*.jpg"`; do mv $picture OutPuts/; done
```
![Déplace toutes les images du répertoire rep_etude ayant l'extension .jpg, quelle que soit la casse des caractères "j", "p" et "g", dans le dossier OutPuts préalablement créé](screenshots/8.png)

9)
```bash
for picture in `find OutPuts/ -type f -iname "*.jpg"`; do chmod 600 $picture; done
```
![Donne les droits de lecture / écriture à l'utilisateur, retire les droits d'exécution à l'utilisateur et retire tous les droits aux autres utilisateurs](screenshots/9.png)

10)
```bash
du -sh rep_etude/
```
![Donne la taille de tous les fichiers du répertoire rep_etude](screenshots/10.1.png)
```bash
let "total = 0"; for line in `find rep_etude/ -type f -o -iname "*.png" -iname "*.jpg" -iname "*.tif" -iname "*.gif" -iname "*.bmp"`; do let "total = $total + `du $line | cut -f 1`"; done; echo "$(($total / 1000))M"
```
![Donne la taille de tous les fichiers de type image dans le répertoire rep_etude/](screenshots/10.2.png)

11)
```bash
for line in `find OutPuts/ -type f -name "*jpg"`; do echo "$line:`shasum $line | cut -d \" \" -f 1`" >> empreintes.txt; done
```
![Enregistre dans le fichier empreintes.txt tous les chemins relatifs des images JPG ainsi que leurs empreintes](screenshots/11.png)

12)
```bash
cat empreintes.txt | cut -d ":" -f 1
```
![Affiche les chemins relatifs des images JPG enregistrées dans le fichier empreintes.txt](screenshots/12.1.png)
```bash
cat empreintes.txt | cut -d ":" -f 2
```
![Affiche les empreintes des images JPG enregistrées dans le fichier empreintes.txt](screenshots/12.2.png)

13)
```bash
for line in `cat empreintes.txt`; do echo `basename $line | cut -d ":" -f 1`; done
```
![Affiche les noms des images JPG enregistrées dans le fichier empreintes.txt, sans leur chemin relatif](screenshots/13.png)

14)
```bash
for line in `find rep_etude/ -iname "*.png" -type f -o -iname "*.jpg" -o -iname "*.tif" -o -iname "*.gif" -o -iname "*.bmp"`; do mv $line OutPuts/; done && let "count = 1"; for line in `find OutPuts/ -type f`; do baseFileName=`basename $line`; mv $line "OutPuts/fichier_${baseFileName:0:3}_num_$count.`echo ${baseFileName#*.} | tr [:upper:] [:lower:]`"; let "count = $count + 1"; done
```
![Déplace toutes les images restantes du répertoire rep_etude dans le répertoire OutPuts, puis renome toutes les images du répertoire OutPuts selon les contraintes posées](screenshots/14.png)

15)
```bash
dico=""; for line in `find OutPuts/ -type f`; do dico="$dico $line:`shasum $line | cut -d \" \" -f 1`"; done
```
![Sauvegarde chaque empreinte liée à une image du répertoire OutPuts dans une variable appelée "dico"](screenshots/15.png)

18)
```bash
for line in `find OutPuts/ -iname "*.png" -type f -o -iname "*.tif" -o -iname "*.gif" -o -iname "*.bmp"`; do magick convert $line "${line%.*}.jpg"; done && for line in `find OutPuts/ -iname "*.png" -type f -o -iname "*.tif" -o -iname "*.gif" -o -iname "*.bmp"`; do rm $line; done
```
![Conversion de toutes les images n'ayant pas l'extension .jpg en image JPG, puis suppression des images n'ayant pas l'extension .jpg](screenshots/18.png)

19)
```bash
mkdir OutPuts_crop/ && for line in `find OutPuts/ -type f -name "*.jpg"`; do magick convert $line -geometry 200x260^ -gravity center -crop 200x260+0+0 "OutPuts_crop/`basename $line | cut -d \"/\" -f 2`"; done
```
![Crée un répertoire OutPuts_crop, puis redimmensionne avec imagemagick toutes les images du répertoire OutPuts](screenshots/19.png)

20)
```bash
pictures=(); for line in `find OutPuts_crop/ -type f -name "*.jpg"`; do pictures+=($line); done && magick montage $pictures -geometry 2048x2048+8 mosaique.jpg
```
![Récupère tous les noms d'images du répertoire OutPuts_crop et constitue une mosaïque de dimension 2048*2048 avec 8 colonnes](screenshots/20.png)

Il se trouve qu'avec la quantité d'images à traiter (plus de 600), la RAM sature très rapidement, même en grand quantité (16Go), il m'est donc impossible d'effectuer le rendu final :
![Erreur pour la question 20](screenshots/20.error.png)
